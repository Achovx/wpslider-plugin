<?php
/*
    Plugin Name: Gwada Slide
    Description: Simple implementation of a slideshow into WordPress
    Author: Andrew Mondor, Bilal Belatoui, Eyram De Souza, Nasfahdine MMADI
    Version: 1.0
*/


// init function
function np_init() {
    $args = array(
        'public' => true,
        'label' => 'Gwada Slides',
        'supports' => array(
            'title',
            'thumbnail',
        ),
    );
    register_post_type('np_images', $args);
}
// Actions are the hooks that the WordPress core launches at specific points during execution, or when specific events occur.
add_action('init', 'np_init');

// including scripts and styles
add_action('wp_print_scripts', 'np_register_scripts');
add_action('wp_print_styles', 'np_register_styles');


// jquery/js files
function np_register_scripts() {
    if (!is_admin()) {
        // register
        wp_register_script('np_nivo-script', plugins_url('nivo-slider/jquery.nivo.slider.js', __FILE__), array( 'jquery' ));
        wp_register_script('np_script', plugins_url('script.js', __FILE__));
 
        // enqueue
        wp_enqueue_script('np_nivo-script');
        wp_enqueue_script('np_script');
    }
}
 
function np_register_styles() {
    // register
    wp_register_style('np_styles', plugins_url('nivo-slider/nivo-slider.css', __FILE__));
    wp_register_style('np_styles_theme', plugins_url('nivo-slider/themes/default/default.css', __FILE__));
 
    // enqueue
    wp_enqueue_style('np_styles');
    wp_enqueue_style('np_styles_theme');
}

//For the shortcode and function we need the same image sizes 
//For the widget a smaller size for the height and width of the images.

add_image_size('np_widget', 680, 350, true);
add_image_size('np_function', 600, 280, true);

//In order to enable post thumbnails
add_theme_support( 'post-thumbnails' );



add_action('add_meta_boxes','init_metabox');
function init_metabox(){
  add_meta_box('url_crea', 'Lien', 'url_crea', 'np_images', 'side');
}

function url_crea($post){
  $url = get_post_meta($post->ID,'_url_crea',true);
  echo '<label for="url_meta">Lien associé :</label>';
  echo '<input id="url_meta" type="url" name="url_site" value="'.$url.'" />';
}

add_action('save_post','save_metabox');
function save_metabox($post_id){
if(isset($_POST['url_site']))
  update_post_meta($post_id, '_url_crea', esc_url($_POST['url_site']));
}





function np_function($type='np_function') {
    $args = array(
        //define carrousel $args
        'post_type' => 'np_images',
        'posts_per_page' => 10
    );
    $result = '<div class="slider-wrapper theme-default">';
    $result .= '<div id="slider" class="nivoSlider">';
 
    //the loop
    $loop = new WP_Query($args);
    while ($loop->have_posts()) {
        $loop->the_post();
        $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);
        $result .='<img title="'.get_the_title().'" src="' . $the_url[0] . '" data-thumb="' . $the_url[0] . '" alt=""/>';
    }


    // return the article caption
    $result .= '</div>';
    $result .='<div id = "htmlcaption" class = "nivo-html-caption">';
    $result .='<strong>This</strong> is an example of a <em>HTML</em> caption with <a href = "#">a link</a>.';
    $result .='</div>';
    $result .='</div>';
    return $result;
}

//shortcode
add_shortcode('np-shortcode', 'np_function');

// widget
function np_widgets_init() {
    register_widget('np_Widget');
}
 
add_action('widgets_init', 'np_widgets_init');

class np_Widget extends WP_Widget {
 
    public function __construct() {
        parent::__construct('np_Widget', 'Nivo Slideshow', array('description' => __('Gwada slide widget ', 'text_domain')));
    }


    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        }
        else {
            $title = __('Gwada Slide', 'text_domain');
        }
        ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = strip_tags($new_instance['title']);
     
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        // the title
        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;
        if (!empty($title))
            echo $before_title . $title . $after_title;
        echo np_function('np_widget');
        echo $after_widget;
    }
}

