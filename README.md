# Gwada Slide Plugin

Gwada Slide Plugin is a wordpress plugin made for a school project. 

We have chosen to create a plugin to create a carousel. Our goal was to provide the user with a modular plugin that can be used throughout the site.


## Installation
You need to have docker desktop installed.
run the command :
```bash
docker-compose up -d
```

## init function
```php
function np_init() {
    $args = array(
        'public' => true,
        'label' => 'Gwada Slides',
        'supports' => array(
            'title',
            'thumbnail',
        ),
    );
    register_post_type('np_images', $args);
}
```
## main function
 
```php
function np_function($type='np_function') {
    $args = array(
        //define carrousel $args
        'post_type' => 'np_images',
        'posts_per_page' => 10
    );
    $result = '<div class="slider-wrapper theme-default">';
    $result .= '<div id="slider" class="nivoSlider">';
 
    //the loop
    $loop = new WP_Query($args);
    while ($loop->have_posts()) {
        $loop->the_post();
        $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);
        $result .='<img title="'.get_the_title().'" src="' . $the_url[0] . '" data-thumb="' . $the_url[0] . '" alt=""/>';
    }


    // return the article caption
    $result .= '</div>';
    $result .='<div id = "htmlcaption" class = "nivo-html-caption">';
    $result .='<strong>This</strong> is an example of a <em>HTML</em> caption with <a href = "#">a link</a>.';
    $result .='</div>';
    $result .='</div>';
    return $result;
}
```

## widget init

```php
function np_widgets_init() {
    register_widget('np_Widget');
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
